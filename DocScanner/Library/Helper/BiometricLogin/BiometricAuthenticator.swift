//
//  EHFAuthenticator.swift
//
//  Created by Christopher Truman on 9/29/14.
//  Copyright (c) 2014 truman. All rights reserved.
//

import Foundation
import LocalAuthentication

typealias EHFCompletionBlock = (Void) ->()
typealias EHFAuthenticationErrorBlock = (Int) -> ()


class BiometricAuthenticator : NSObject {
    
    var context : LAContext
    
    // reason string presented to the user in auth dialog
    var reason : NSString
    
    // Allows fallback button title customization. If set to nil, "Enter Password" is used.
    var fallbackButtonTitle : NSString
    
    // If set to NO it will not customize the fallback title, shows default "Enter Password".  If set to YES, title is customizable.  Default value is NO.
    var useDefaultFallbackTitle : Bool
    
    // Disable "Enter Password" fallback button. Default value is NO.
    var hideFallbackButton : Bool
    
    // Default value is LAPolicyDeviceOwnerAuthenticationWithBiometrics.  This value will be useful if LocalAuthentication.framework introduces new auth policies in future version of iOS.
    var policy : LAPolicy
    
    
    class var sharedInstance : BiometricAuthenticator {
        struct Static {
            static let instance : BiometricAuthenticator = BiometricAuthenticator()
        }
        return Static.instance
    }
    
    override init(){
        self.context = LAContext()
        self.fallbackButtonTitle = ""
        self.useDefaultFallbackTitle = false
        self.hideFallbackButton = false
        if #available(iOS 9.0, *) {
            self.policy = .deviceOwnerAuthentication
        } else {
            // Fallback on earlier versions
            self.policy = .deviceOwnerAuthenticationWithBiometrics
        }
        self.reason = ""
    }
    
    class func canAuthenticateWithError(_ error: NSErrorPointer) -> Bool{
        
        if (UserDefaults.standard.object(forKey: "touch_id_security") != nil){
            
            if UserDefaults.standard.bool(forKey: "touch_id_security") == false{
                return false
            }
        }
        else {
            return false
        }
        if ((NSClassFromString("LAContext")) != nil){
            if (BiometricAuthenticator.sharedInstance.context .canEvaluatePolicy(BiometricAuthenticator.sharedInstance.policy, error: nil)){
                return true
            }
            return false
        }
        return false
    }

    func authenticateWithSuccess(_ success:  @escaping EHFCompletionBlock, failure:  @escaping EHFAuthenticationErrorBlock){
   
        self.context = LAContext()
        var authError : NSError?
        if (self.useDefaultFallbackTitle) {
            self.context.localizedFallbackTitle = self.fallbackButtonTitle as String;
        }else if (self.hideFallbackButton){
            self.context.localizedFallbackTitle = "";
        }
        if (self.context.canEvaluatePolicy(policy, error: &authError)) {
            self.context.evaluatePolicy(policy, localizedReason:
                reason as String, reply:{ authenticated, error in
                if (authenticated) {
                    DispatchQueue.main.async(execute: {success()})
                } else {
                    DispatchQueue.main.async(execute: {failure(error!._code)})
                }
            })
        } else {
            failure(authError!.code)
        }
    }
    /*
    func executeBioametricAuthentication(msg: String?, success:  @escaping ((_ result:Bool) -> Void),  failure: @escaping ((_ failed: Int) -> Void))
    {
        self.context = LAContext()
        var authError : NSError?
        if (self.useDefaultFallbackTitle) {
            self.context.localizedFallbackTitle = self.fallbackButtonTitle as String;
        }else if (self.hideFallbackButton){
            self.context.localizedFallbackTitle = "";
        }
        if (self.context.canEvaluatePolicy(policy, error: &authError))
        {
            self.context.evaluatePolicy(policy, localizedReason:
                reason as String, reply:{ authenticated, error in
                    if (authenticated) {
                        DispatchQueue.main.async(execute: {
                            success(true)
                        })
                    } else {
                        DispatchQueue.main.async(execute: {failure(error!._code)})
                    }
            })
        } else {
            failure(authError!.code)
        }
    }
 */
    
    
    
}
