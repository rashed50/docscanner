//
//  Constants.h
//  FutureVault
//
//  Created by Mesbah Uddin on 6/7/16.
//  Copyright © 2016 FutureVault Inc. All rights reserved.
//

#import <Foundation/Foundation.h>


 
#define APP_NAME  @"PDF Generator"
#define DOWNLOAD_File_NAME   @"pdfgenerator"
#define PDF_File_TYPE   @"pdf"
#define MSWORD_File_TYPE   @"doc"
#define MSWORD_File_TYPE10   @"docx"
#define TEXT_File_TYPE   @"txt"
#define MSEXCEL_File_TYPE   @"xls"
#define MSEXCEL_File_TYPE10   @"xlsx"
#define PNG_IMAGE_File_TYPE   @"png"
#define JPG_IMAGE_File_TYPE   @"jpg"
#define JPEG_IMAGE_File_TYPE   @"jpeg"
#define GIF_IMAGE_File_TYPE   @"gif"

#define SCREEN_WIDTH    [[UIScreen mainScreen] bounds].size.width
#define SCREEN_HEIGHT   [[UIScreen mainScreen] bounds].size.height

#define APP_HEXA_COlOR_CODE @"3870A8"
// rgb 56,112,168

#define NotReachableErrorCode 44444
#define TimeoutErrorCode 44445
#define APIResponseErrorCode 44446
#define TwoStepVerificationRequiredCode 22222
#define UnauthorizedErrorCode 401
 

#define APP_COlOR_CODE [UIColor colorWithRed:0.22 green:0.44 blue:0.66 alpha:1.0]


extern NSString *const kMainTabBarControllerIdentifier;
extern NSString *const kTermsAndConditionsViewControllerIdentifier;
extern NSString *const kAllFileUploadedNotificationName;
