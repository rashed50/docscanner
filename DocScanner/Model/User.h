//
//  User.h
//  FutureVault
//
//  Created by Mesbah Uddin on 6/7/16.
//  Copyright © 2016 FutureVault Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject

@property (nonatomic, strong) NSString *userID;
@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *middleName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *fullName;
@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) NSString *email;
 

-(instancetype) initWithDictionary:(NSDictionary *)userDictionary;
@end

