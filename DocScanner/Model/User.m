//
//  User.m
//  FutureVault
//
//  Created by Mesbah Uddin on 6/7/16.
//  Copyright © 2016 FutureVault Inc. All rights reserved.
//

#import "User.h"

@implementation User

-(instancetype) initWithDictionary:(NSDictionary *)userDictionary {
    if (self=[super init]) {
        NSDictionary *userInfoDictionary = userDictionary[@"userInfo"];
        
        if (userInfoDictionary) {
            _userID = userInfoDictionary[@"id"];
            _firstName = userInfoDictionary[@"firstname"];
            _middleName = userInfoDictionary[@"middlename"];
            _lastName = userInfoDictionary[@"lastname"];
            _fullName = userInfoDictionary[@"name"];
            _userName = userInfoDictionary[@"username"];
            _email = userInfoDictionary[@"email"];
            
        }
    }
    
    return self;
}

@end
