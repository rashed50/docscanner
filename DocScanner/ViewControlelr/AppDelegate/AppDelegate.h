//
//  AppDelegate.h
//  DocScanner
//
//  Created by Rashed on 2017-01-16.
//  Copyright © 2017 Rashed. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMDrawerController.h"
 

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property MMDrawerController *drawerController;

 

@end

