//
//  AppDelegate.m
//  DocScanner
//
//  Created by Rashed on 2017-01-16.
//  Copyright © 2017 Rashed. All rights reserved.
//

#import "AppDelegate.h"
#import "MMDrawerController.h"
#import "MMExampleDrawerVisualStateManager.h"
#import "DocumentScanner-Swift.h"
 

//#import "DocScanner-Bridging-Header.h"
// app  corlor code 3870A8


// pod customization By Rashed
/*
    Pod Name: SwiftOCR
    Update : set SwiftOCR  swift version 4.2
*/

/*
 Pod Name : MBDocCapture,
 Controller :  EditScanViewController
 Method Name: viewDidLoad
 Added Code:
 
                let attrs = [
                            NSAttributedString.Key.foregroundColor:UIColor.white,NSAttributedString.Key.font: UIFont(name: "Georgia-Bold", size:18)!
                            ]
               self.navigationController?.navigationBar.titleTextAttributes = attrs
               self.navigationController?.navigationBar.barTintColor =   UIColor(red: 56/255.0, green: 112/255.0, blue: 168/255.0, alpha: 1.0)
 */

/*
    Pod Name : MBDocCapture,
    Controller :  imageScannerController
    Method Name: public required init(image: UIImage? = nil, delegate:           ImageScannerControllerDelegate? = nil)
    Added Code:
                 navigationBar.barTintColor = UIColor.init("3870A8")
        
*/




@interface AppDelegate ()
@end

@implementation AppDelegate


   

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
     [Helper createDirectory];
   // [Helper clearAllTemporaryFile];

    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


-(void)application:(UIApplication *)application handleEventsForBackgroundURLSession:(nonnull NSString *)identifier completionHandler:(nonnull void (^)(void))completionHandler{
    
    
    
}
/*

func application(_ application: UIApplication,
                  handleEventsForBackgroundURLSession handleEventsForBackgroundURLSessionidentifier: String,
                  completionHandler: @escaping () -> Void) {
   backgroundSessionCompletionHandler = completionHandler
 }

*/





@end
