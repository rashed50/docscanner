//
//  AppSettingViewController.swift
//  DocScanner
//
//  Created by Rashedul Hoque on 27/3/20.
//  Copyright © 2020 Rashed. All rights reserved.
//

import UIKit
import MessageUI
import GoogleMobileAds

class AppSettingViewController: UIViewController {
    var adMobBannerView: GADBannerView!
    var settingList = [String]()
    
    
    @IBOutlet weak var pdfDocPasswordTxt: UITextField!
    
    @IBOutlet weak var autoOnOffOutlet: UISwitch!
    
    @IBOutlet weak var ocrPhotoSaveSwitch: UISwitch!
    @IBAction func AutoCropOnOffClick(_ sender: UISwitch) {
        
        if sender.isOn == true{
            Helper.setUserDefaultPreference(autoCropOnOff:"1")
        }
        else if sender.isOn == false{
             Helper.setUserDefaultPreference(autoCropOnOff:"0")
        }
    }
    
    @IBAction func OCRCameraPhotoSaveAction(_ sender: UISwitch) {
        
        if sender.isOn == true{
            Helper.setOCRCameraPhotoSaveInLibrary(OnOff: "1")
        }
        else if sender.isOn == false{
              Helper.setOCRCameraPhotoSaveInLibrary(OnOff: "0")
        }
   }
    
    
    @IBAction func feedbackAndSuggestionAction(_ sender: Any) {
         
        Helper.sendEmail(viewCon: self, fileUrl:nil ,operationType: 1)
    }
    
    
    @IBAction func tellFriendAction(_ sender: Any) {
        
       Helper.sendEmail(viewCon: self, fileUrl:nil ,operationType: 2)
    }
    
    @IBAction func developerAppsAction(_ sender: Any) {
        
        let story = UIStoryboard.init(name: "MyAppMain", bundle: nil)
        var viewCon : UIViewController
        if #available(iOS 13.0, *) {
              viewCon = story.instantiateViewController(identifier: "pageContainerViewController")
        } else {
           viewCon = story.instantiateViewController(withIdentifier: "pageContainerViewController")
        }
        
        self.navigationController?.pushViewController(viewCon, animated: true)
         
    }
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeGoogleAdmob()
        NotificationCenter.default.addObserver(self, selector: #selector(FileDownloadViewController.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(FileDownloadViewController.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        self.addDoneButtonOnKeyboard()
       
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        Helper.setNavigationBarProperty(navbar: self.navigationController!, size: 18, title:NSLocalizedString("App Setting", comment: ""))
        autoOnOffOutlet.isOn =   Helper.getUserDefaultPreference()
        pdfDocPasswordTxt.text = Helper.getPdfDocumentPassword()
        ocrPhotoSaveSwitch.isOn = Helper.getOCRCameraPhotoSaveInLibrary()
         self.tabBarController?.tabBar.isHidden = false
    }
    
    
    func addDoneButtonOnKeyboard(){
           
           let doneToolbar = UIToolbar.init()
           let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
           let done: UIBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Done", comment: ""), style: .done, target: self, action: #selector(self.doneButtonAction))
          let back: UIBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Back", comment: ""), style: .plain, target: self, action: #selector(self.backButtonAction))
           let items = [flexSpace,back,done]
           doneToolbar.items = items
           doneToolbar.sizeToFit()
           self.pdfDocPasswordTxt.inputAccessoryView = doneToolbar
       }

       @objc func doneButtonAction(){
           self.pdfDocPasswordTxt.resignFirstResponder()
           Helper.setPdfDocumentPassword(password: self.pdfDocPasswordTxt.text ?? "")
       }
    @objc func backButtonAction(){
        self.pdfDocPasswordTxt.resignFirstResponder()
         
    }
       @objc  func keyboardWillShow(_ notification: Notification) {
          // let userInfo = (notification as NSNotification).userInfo!
        //   let keyboardSize = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
           //self.scrollViewBottomConstraint.constant = keyboardSize.size.height-50
        }

       @objc func keyboardWillHide(_ notification: Notification) {
               //self.scrollViewBottomConstraint.constant = 0
       }
    
    
    func initializeGoogleAdmob() {
                                
         adMobBannerView = GADBannerView(adSize: kGADAdSizeBanner)
         adMobBannerView.adUnitID = Helper.getAdMobBannerID()
         adMobBannerView.rootViewController = self
         adMobBannerView.delegate = self as GADBannerViewDelegate
         adMobBannerView.load(GADRequest())
         addBannerInViewToBottom()
                           
    }
                  
   func addBannerInViewToBottom() {
          
          adMobBannerView.frame = CGRect(x: 0.0,
                                         y: self.view.frame.height - adMobBannerView.frame.height - 80,
                                                     width: self.view.frame.width,
                                    height: adMobBannerView.frame.height)
       
          self.view.addSubview(adMobBannerView)
      }
     
}

extension AppSettingViewController : GADBannerViewDelegate{
    
      /// Tells the delegate an ad request failed.
         func adView(_ bannerView: GADBannerView,
             didFailToReceiveAdWithError error: GADRequestError) {
           print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
         }

         /// Tells the delegate that a full-screen view will be presented in response
         /// to the user clicking on an ad.
         func adViewWillPresentScreen(_ bannerView: GADBannerView) {
           print("adViewWillPresentScreen")
         }

         /// Tells the delegate that the full-screen view will be dismissed.
         func adViewWillDismissScreen(_ bannerView: GADBannerView) {
           print("adViewWillDismissScreen")
         }

         /// Tells the delegate that the full-screen view has been dismissed.
         func adViewDidDismissScreen(_ bannerView: GADBannerView) {
           print("adViewDidDismissScreen")
         }

         /// Tells the delegate that a user click will open another app (such as
         /// the App Store), backgrounding the current app.
         func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
           print("adViewWillLeaveApplication")
         
         }
}


extension AppSettingViewController : MFMailComposeViewControllerDelegate{
   
     public func mailComposeController(_ controller: MFMailComposeViewController,
                                      didFinishWith result: MFMailComposeResult,
                                      error: Error?) {
        switch (result) {
        case .cancelled:
            controller.dismiss(animated: true, completion: nil)
        case .sent:
            controller.dismiss(animated: true, completion: nil)
        case .failed:
            controller.dismiss(animated: true, completion: {
               
                let sendMailErrorAlert = UIAlertController.init(title: NSLocalizedString("Error", comment: ""),
                                                                message: "Unable to send email. Please check your email and Internet Connection " +
                    "settings and try again.", preferredStyle: .alert)
                sendMailErrorAlert.addAction(UIAlertAction.init(title: NSLocalizedString("OK", comment: ""),
                                                                style: .default, handler: nil))
                controller.present(sendMailErrorAlert, animated: true, completion: nil)
            })
        default:
            break;
        }
     }

}
