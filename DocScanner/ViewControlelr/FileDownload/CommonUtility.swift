/// Copyright (c) 2019 Razeware LLC
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy
/// of this software and associated documentation files (the "Software"), to deal
/// in the Software without restriction, including without limitation the rights
/// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
/// copies of the Software, and to permit persons to whom the Software is
/// furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in
/// all copies or substantial portions of the Software.
/// 
/// Notwithstanding the foregoing, you may not use, copy, modify, merge, publish,
/// distribute, sublicense, create a derivative work, and/or sell copies of the
/// Software in any work that is designed, intended, or marketed for pedagogical or
/// instructional purposes related to programming, coding, application development,
/// or information technology.  Permission for such use, copying, modification,
/// merger, publication, distribution, sublicensing, creation of derivative works,
/// or sale is expressly withheld.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
/// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
/// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
/// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
/// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
/// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
/// THE SOFTWARE.

import Foundation.NSURL

//
// MARK: - CommonUtility
//

/// Query service creates Track objects
class CommonUtility {
  //
  // MARK: - Constants
  //
 
  
  //
  // MARK: - Variables And Properties
  //
   
  
  //
  // MARK: - Initialization
  //
  
  
  
  static func GetFileLocation(fileName: String) -> URL {
   
        let documentsPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!//.appendingPathComponent("AllFiles")
       return documentsPath
   }
   
   
  static  func SearchAllFile() -> [FileInfo] {
      
      var allList = [URL]()
      var allFileList = [FileInfo]()
      let docParh = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent("AllFiles")
    
      do {
          allList = try FileManager.default.contentsOfDirectory(at: GetFileLocation(fileName: ""), includingPropertiesForKeys: nil, options:.skipsHiddenFiles)
        print("all \(allList)")
        for aa in allList {
          var spitlist = [String]()
          spitlist = aa.absoluteString.components(separatedBy:"/")
          let f = FileInfo.init(name: spitlist.last!, type: "pdf", url: aa)
          allFileList.append(f)
          
        }
      
      } catch  {
        print("Error")
      }
      
 
      
      
      return allFileList
    }
     
}
