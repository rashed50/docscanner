//
//  HistoryViewController.swift
//  Doc Scanner
//
//  Created by Rashed on 2017-01-17.
//  Copyright © 2017 iRLMobile. All rights reserved.
//

import UIKit
import MessageUI
import PDFReader
import GoogleMobileAds

class HistoryViewController: UIViewController,UITableViewDelegate,ReaderViewControllerDelegate,UITableViewDataSource,GADBannerViewDelegate {

         var adMobBannerView: GADBannerView!
    var allDocumentInfoDic :NSMutableDictionary = NSMutableDictionary()
    
  
    let cellReuseIdentifier = "cellInfo"
    
    // don't forget to hook this up from the storyboard
    
    @IBOutlet var documentListTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        documentListTableView.delegate = self
        documentListTableView.dataSource = self
        documentListTableView.sectionHeaderHeight = 30
        documentListTableView.estimatedSectionHeaderHeight = UITableView.automaticDimension
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
          self.documentListTableView.addGestureRecognizer(longPress)
        self.title = NSLocalizedString("Storage" , comment: "")
        initializeGoogleAdmob()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        Helper.setNavigationBarProperty(navbar: self.navigationController!, size: 18, title:NSLocalizedString("File Storage", comment: ""))
        allDocumentInfoDic =  Helper.getAllFile()
        self.documentListTableView.reloadData()
      
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()        
    }
    
/// MARK TABLE VIEW DELEGATE METHODS
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:UITableViewCell = (self.documentListTableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier))!
        
        
        let allSectionList :NSArray = allDocumentInfoDic.allKeys as NSArray
        var sectionTitle = ""
        sectionTitle = allSectionList[indexPath.section] as! String;
        
        let listData =  (allDocumentInfoDic.value(forKey: sectionTitle)) as! NSArray
        
        
         let iconView :UIImageView = cell.viewWithTag(10) as! UIImageView
         let nameLbl :UILabel = cell.viewWithTag(11) as! UILabel
        nameLbl.text = listData[indexPath.row] as? String
       // iconView.image = UIImage.init(named: "")
        
        let afile = (listData[indexPath.row] as? String)?.split(separator: ".").last ?? "pdf"
        let ty : String = (String(afile) as String)
        
           if(ty == PDF_File_TYPE)
           {
                  iconView.image = UIImage.init(named: "pdf")!
           }else if(ty == MSWORD_File_TYPE){
                 iconView.image = UIImage.init(named: "msword.png")!
           }
          else if(ty == TEXT_File_TYPE){
                  iconView.image = UIImage.init(named: "text.png")!
            }
           else if(ty == JPG_IMAGE_File_TYPE || ty == "jpeg"  || ty == PNG_IMAGE_File_TYPE || ty == "gif"){
            
                 iconView.image = UIImage.init(named: "image")!
            }
           else{
               iconView.image = nil;
           }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
         let allSectionList :NSArray = allDocumentInfoDic.allKeys as NSArray
         return allSectionList[section] as? String
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        let allSectionList :NSArray = allDocumentInfoDic.allKeys as NSArray
        return allSectionList.count;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        let allSectionList :NSArray = allDocumentInfoDic.allKeys as NSArray
        var sectionTitle = ""
        sectionTitle = allSectionList[section] as! String;
        
        let listData =  (allDocumentInfoDic.value(forKey: sectionTitle)) as! NSArray
        
        return  listData.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        /*
        NSString *filePath =[allBookPathArray objectAtIndex:indexPath.row];
        
        NSString *phrase = nil;
        ReaderDocument *document = [ReaderDocument withDocumentFilePath:filePath password:phrase];
        
        if (document != nil)
        {
            ReaderViewController *readerViewController = [[ReaderViewController alloc] initWithReaderDocument:document];
            
            readerViewController.delegate = self;
            
            #if (DEMO_VIEW_CONTROLLER_PUSH == TRUE)
                
                [self.navigationController pushViewController:readerViewController animated:YES];
                
            #else // present in a modal view controller
                
                readerViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                readerViewController.modalPresentationStyle = UIModalPresentationFullScreen;
                
                [self presentViewController:readerViewController animated:YES completion:NULL];
                
            #endif // DEMO_VIEW_CONTROLLER_PUSH
        }
        else // Log an error so that we know that something went wrong
        {
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Error !" message:@"May be you have no access permission. or Fil is not exists " delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            [alertView show];
            NSLog(@"%s [ReaderDocument withDocumentFilePath:'%@' password:'%@'] failed.", __FUNCTION__, filePath, phrase);
        }
        
        */
        
        let allSectionList :NSArray = allDocumentInfoDic.allKeys as NSArray
        var sectionTitle = ""
        sectionTitle = allSectionList[indexPath.section] as! String;
        
        let listData =  (allDocumentInfoDic.value(forKey: sectionTitle)) as! NSArray
        
        var filePath : String = String(listData[indexPath.row] as! String)
        filePath =  Helper.getFilePathWithExtention("\(sectionTitle)/\(filePath)")
        
        let ( fname,type,url, afile) = Helper.getFileNameAndTypeFromFilePath(path: filePath)
        print(filePath)
         
        Helper.openSelectedFileBy(tabbar: self.tabBarController!,nextTabIndex:3 , view: self, afile: afile,navCon: self.navigationController!)
        
        
    }
    
  // MARK ReaderViewController Delegate Methods

    //  - (void)dismissReaderViewController:(ReaderViewController *)viewController;
   
    
  @objc func handleLongPress(sender: UILongPressGestureRecognizer) {

      if sender.state == UIGestureRecognizer.State.began {
          let touchPoint = sender.location(in: documentListTableView)
          if documentListTableView.indexPathForRow(at: touchPoint) != nil {

            let indexpath : IndexPath = documentListTableView.indexPathForRow(at: touchPoint)!
            
            let allSectionList :NSArray = allDocumentInfoDic.allKeys as NSArray
                   var sectionTitle = ""
                   sectionTitle = allSectionList[indexpath.section] as! String;
                   
                   let listData =  (allDocumentInfoDic.value(forKey: sectionTitle)) as! NSArray
                   
                   var filePath : String = String(listData[indexpath.row] as! String)
                   filePath =  Helper.getFilePathWithExtention("\(sectionTitle)/\(filePath)")
            
             // let afile :FileInfo = allFilesList[documentListTableView.indexPathForRow(at: touchPoint)!.row]
           // Helper.sharePressed(view: self, fileurl: filePath.url)
            
            let ( fname,type,url, afile) = Helper.getFileNameAndTypeFromFilePath(path: filePath)
                   print(filePath)
            
            Helper.sharePressed(tabbar: self.tabBarController!, nextTabIndex:4, view: self, afile: afile)
              print("Long press Pressed:)")
              
              
          }
      }


  }
  
    
}
 

extension HistoryViewController{

    func dismiss(_ viewController: ReaderViewController!) {
         print("Long press Pressed:")
    }

    
    //
    // MARK GOOGLE ADMOB BANNER DELEGATE
    //
      
      
      func initializeGoogleAdmob() {
                       
                adMobBannerView = GADBannerView(adSize: kGADAdSizeBanner)
                adMobBannerView.adUnitID = Helper.getAdMobBannerID()
                adMobBannerView.rootViewController = self
                adMobBannerView.delegate = self as GADBannerViewDelegate
                adMobBannerView.load(GADRequest())
               // adMobBannerView.isHidden = true
                addBannerInViewToBottom()
                  
         }
         
      func addBannerInViewToBottom() {
             
             adMobBannerView.frame = CGRect(x: 0.0,
                                            y: self.view.frame.height - adMobBannerView.frame.height-100,
                                                        width: self.view.frame.width,
                                      height: adMobBannerView.frame.height)
          
             self.view.addSubview(adMobBannerView)
         }
    
    /// Tells the delegate an ad request failed.
       func adView(_ bannerView: GADBannerView,
           didFailToReceiveAdWithError error: GADRequestError) {
         print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
       }

       /// Tells the delegate that a full-screen view will be presented in response
       /// to the user clicking on an ad.
       func adViewWillPresentScreen(_ bannerView: GADBannerView) {
         print("adViewWillPresentScreen")
       }

       /// Tells the delegate that the full-screen view will be dismissed.
       func adViewWillDismissScreen(_ bannerView: GADBannerView) {
         print("adViewWillDismissScreen")
       }

       /// Tells the delegate that the full-screen view has been dismissed.
       func adViewDidDismissScreen(_ bannerView: GADBannerView) {
         print("adViewDidDismissScreen")
       }

       /// Tells the delegate that a user click will open another app (such as
       /// the App Store), backgrounding the current app.
       func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
         print("adViewWillLeaveApplication")
       
       }
    
}
extension HistoryViewController : MFMailComposeViewControllerDelegate{
    
    public func mailComposeController(_ controller: MFMailComposeViewController,
                                       didFinishWith result: MFMailComposeResult,
                                       error: Error?) {
         switch (result) {
         case .cancelled:
             controller.dismiss(animated: true, completion: nil)
         case .sent:
             controller.dismiss(animated: true, completion: nil)
         case .failed:
             controller.dismiss(animated: true, completion: {
                
                 let sendMailErrorAlert = UIAlertController.init(title: NSLocalizedString("Error", comment: ""),
                                                                 message: "Unable to send email. Please check your email and Internet Connection " +
                     "settings and try again.", preferredStyle: .alert)
                 sendMailErrorAlert.addAction(UIAlertAction.init(title: NSLocalizedString("OK", comment: ""),
                                                                 style: .default, handler: nil))
                 controller.present(sendMailErrorAlert, animated: true, completion: nil)
             })
         default:
             break;
         }
     }
}
