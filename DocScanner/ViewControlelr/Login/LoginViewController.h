//
//  LoginViewController.h
//  FutureVault
//
//  Created by Mesbah Uddin on 6/7/16.
//  Copyright © 2016 FutureVault Inc. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface LoginViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *usernameField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *loginBottomConstraint;
@property (weak, nonatomic) IBOutlet UIScrollView *inputScrollView;
@property (weak, nonatomic) IBOutlet UIButton *signinButton;
//@property (weak, nonatomic) IBOutlet KILabel *appInfoUrlLabel;


@end
