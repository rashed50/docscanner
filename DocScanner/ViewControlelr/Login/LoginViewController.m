//
//  LoginViewController.m
//  FutureVault
//
//  Created by Mesbah Uddin on 6/7/16.
//  Copyright © 2016 FutureVault Inc. All rights reserved.
//



#import "LoginViewController.h"
#import "NavigationManager.h"
#import "DocScanner-Bridging-Header.h"


@interface LoginViewController () {
    id keyboardWillShowNotification;
    id keyboardWillHideNotification;
}

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
   
}


- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    self.usernameField.text =  @"";
    self.passwordField.text =  @"";
     //[self loginButtonPressed:nil];
   // [self checkBiometricAuthentication];
    

    [self.usernameField resignFirstResponder];
    [self.passwordField resignFirstResponder];
    self.loginBottomConstraint.constant = 100;
    
    keyboardWillShowNotification = [[NSNotificationCenter defaultCenter] addObserverForName:UIKeyboardDidShowNotification object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {
        NSDictionary* userInfo = [note userInfo];
        CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
        
        [UIView animateWithDuration:.1 animations:^{
            self.loginBottomConstraint.constant = keyboardSize.height;
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
            [self.inputScrollView scrollRectToVisible:[self.signinButton frame] animated:YES];
        }];
        
    }];
    
    keyboardWillHideNotification = [[NSNotificationCenter defaultCenter] addObserverForName:UIKeyboardWillHideNotification object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {
        self.loginBottomConstraint.constant = 100;
    }];
}

-(void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self.usernameField resignFirstResponder];
    [self.passwordField resignFirstResponder];

    [[NSNotificationCenter defaultCenter] removeObserver:keyboardWillShowNotification];
    [[NSNotificationCenter defaultCenter] removeObserver:keyboardWillHideNotification];
}

- (IBAction)loginButtonPressed:(id)sender {
    
    
   }

 

#pragma mark - TextField Delegate UITextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField {
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.usernameField) {
        [self.passwordField becomeFirstResponder];
        [self.inputScrollView scrollRectToVisible:self.passwordField.frame animated:YES];
    } else if (textField == self.passwordField) {
        [self loginButtonPressed:nil];
    }
    return YES;
}


#pragma mark USER DEFINE METHODS


-(void)clearLoginTextFieldData{
    self.usernameField.text = @"";
    self.passwordField.text = @"";
}
-(void)setUserInfoToKeychain
{
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:true] forKey:@"touch_id_security"];
    [[NSUserDefaults standardUserDefaults] setObject:self.usernameField.text forKey:@"userName"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[[KeychainWrapper alloc] init] mySetObject:self.passwordField.text forKey:(id)kSecValueData];
     

}

-(void)checkBiometricAuthentication
{
    [[BiometricAuthenticator sharedInstance] setReason:@"Authentication is needed to access your app"];
    [[BiometricAuthenticator sharedInstance] setFallbackButtonTitle:@"Enter Password"];
    [[BiometricAuthenticator sharedInstance] setUseDefaultFallbackTitle:NO];


    NSError *error;
    if ([BiometricAuthenticator canAuthenticateWithError:&error]){
        [self startBiometricAuthentication];
    }
    else {
        NSString * authErrorString = @"Check your Touch ID Settings.";
        switch (error.code) {
            case LAErrorTouchIDNotEnrolled:
                authErrorString = @"No Touch ID fingers enrolled.";
                break;
            case LAErrorTouchIDNotAvailable:
                authErrorString = @"Touch ID not   on your device.";
                break;
            case LAErrorPasscodeNotSet:
                authErrorString = @"Need a passcode set to use Touch ID.";
                break;
            default:
                authErrorString = @"Check your Touch ID Settings.";
                break;
        }
    }

}

-( void)startBiometricAuthentication {


    [[BiometricAuthenticator sharedInstance] authenticateWithSuccess:^(void){

        KeychainWrapper *keychain =[[KeychainWrapper alloc] init];
        NSLog(@"user name %@",[NSString stringWithFormat:@"%@",[keychain myObjectForKey:(id)kSecValueData]]);
        self.usernameField.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"userName"];
        self.passwordField.text = [NSString stringWithFormat:@"%@",[keychain myObjectForKey:(id)kSecValueData]];
        [self loginButtonPressed:nil];

    } failure:^(NSInteger errorCode){

        NSString * authErrorString;
        switch (errorCode) {
            case LAErrorSystemCancel:
                authErrorString = @"System canceled auth request due to app coming to foreground or background.";
                break;
            case LAErrorAuthenticationFailed:
                authErrorString = @"User failed after a few attempts.";
                break;
            case LAErrorUserCancel:
                authErrorString = @"User cancelled.";
                break;

            case LAErrorUserFallback:
                authErrorString = @"Fallback auth method should be implemented here.";
                break;
            case LAErrorTouchIDNotEnrolled:
                authErrorString = @"No Touch ID fingers enrolled.";
                break;
            case LAErrorTouchIDNotAvailable:
                authErrorString = @"Touch ID not available on your device.";
                break;
            case LAErrorPasscodeNotSet:
                authErrorString = @"Need a passcode set to use Touch ID.";
                break;
            default:
                authErrorString = @"Check your Touch ID Settings.";
                break;
        }

    }];

}

@end
