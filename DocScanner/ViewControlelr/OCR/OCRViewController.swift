//
//  OCRViewController.swift
//  DocScanner
//
//  Created by Rashedul Hoque on 5/4/20.
//  Copyright © 2020 Rashed. All rights reserved.
//

import UIKit
import MobileCoreServices
import TesseractOCR
import GPUImage
import SwiftOCR
import GoogleMobileAds



 class OCRViewController: UIViewController,GADBannerViewDelegate {
  
    var adMobBannerView: GADBannerView!
    
    @IBOutlet weak var scrollViewBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var ocrTextView: UITextView!
    var isOpenCameraView : Bool = false
    @IBOutlet weak var selectButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    
    
    @IBAction func SaveBtnAction(_ sender: Any) {
        self.SaveOCRDocumentAsFile()
       
    }
    
    @IBAction func addNewButtonAction(_ sender: Any) {
        
        self.TakePhotoOfDocument()
    }
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeGoogleAdmob()
        Helper.setNavigationBarProperty(navbar: self.navigationController!, size: 18, title:NSLocalizedString("OCR Scanner", comment: ""))
        selectButton.setTitle(NSLocalizedString("Add File", comment: ""), for: .normal)
        saveButton.setTitle(NSLocalizedString("Save", comment: ""), for: .normal)
        NotificationCenter.default.addObserver(self, selector: #selector(FileDownloadViewController.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(FileDownloadViewController.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        self.addDoneButtonOnKeyboard()
        
    }
     
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        ocrTextView.isUserInteractionEnabled = false
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
          super.viewDidAppear(animated)
          //   callPdfToText()
      }
    
    
    //
       // MARK GOOGLE ADMOB BANNER DELEGATE
       //
         
         
         func initializeGoogleAdmob() {
                          
                   adMobBannerView = GADBannerView(adSize: kGADAdSizeBanner)
                   adMobBannerView.adUnitID = Helper.getAdMobBannerID()
                   adMobBannerView.rootViewController = self
                   adMobBannerView.delegate = self as GADBannerViewDelegate
                   adMobBannerView.load(GADRequest())
                  // adMobBannerView.isHidden = true
                   addBannerInViewToBottom()
                     
            }
            
         func addBannerInViewToBottom() {
                
                adMobBannerView.frame = CGRect(x: 0.0,
                                               y: self.view.frame.height - adMobBannerView.frame.height-100,
                                                           width: self.view.frame.width,
                                         height: adMobBannerView.frame.height)
             
                self.view.addSubview(adMobBannerView)
            }
       
       /// Tells the delegate an ad request failed.
          func adView(_ bannerView: GADBannerView,
              didFailToReceiveAdWithError error: GADRequestError) {
            print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
          }

          /// Tells the delegate that a full-screen view will be presented in response
          /// to the user clicking on an ad.
          func adViewWillPresentScreen(_ bannerView: GADBannerView) {
            print("adViewWillPresentScreen")
          }

          /// Tells the delegate that the full-screen view will be dismissed.
          func adViewWillDismissScreen(_ bannerView: GADBannerView) {
            print("adViewWillDismissScreen")
          }

          /// Tells the delegate that the full-screen view has been dismissed.
          func adViewDidDismissScreen(_ bannerView: GADBannerView) {
            print("adViewDidDismissScreen")
          }

          /// Tells the delegate that a user click will open another app (such as
          /// the App Store), backgrounding the current app.
          func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
            print("adViewWillLeaveApplication")
          
          }
    
    func addDoneButtonOnKeyboard(){
        
        let doneToolbar = UIToolbar.init()
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Done", comment: ""), style: .done, target: self, action: #selector(self.doneButtonAction))
        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        self.ocrTextView.inputAccessoryView = doneToolbar
    }

    @objc func doneButtonAction(){
        self.ocrTextView.resignFirstResponder()
    }
    @objc  func keyboardWillShow(_ notification: Notification) {
        let userInfo = (notification as NSNotification).userInfo!
        let keyboardSize = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        self.scrollViewBottomConstraint.constant = keyboardSize.size.height-50
     }

    @objc func keyboardWillHide(_ notification: Notification) {
            self.scrollViewBottomConstraint.constant = 0
    }
    func showLoading(msg: String) {
         MBProgressHUD.showAdded(to: self.view, animated: true)
    }
   
    func hideHud() {
        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
    }
 
    func callPdfToText() {
    
        let path = Helper.getOCRDocumentTemporarySavingPath("temp.pdf")
        var imagelist = [UIImage]()
        imagelist = Helper.drawOnPDF(path: path)
        self.showLoading(msg: "OCR is Processing")
    
    //for i in 0..<imagelist.count{
        self.performImageRecognition(imagelist[0])
      //  }
        self.hideHud()
    }
     
    
    
    
    func SaveOCRDocumentAsFile()  {
                //Step : 1
                let alert = UIAlertController(title: NSLocalizedString("File Name", comment: ""), message: "", preferredStyle: UIAlertController.Style.alert )
                //Step : 2
                let save = UIAlertAction(title:NSLocalizedString("Save", comment: "") , style: .default) { (alertAction) in
                    
                    let textField = alert.textFields![0] as UITextField
                    var fname : String =  textField.text ?? ""
                    if textField.text == "" {
                        fname = Helper.getTodayDate()
                    }
                    _ = Helper.saveOCRResult(ocrResultStr: self.ocrTextView.text, fileName: fname , fileType: "txt")
                    self.ocrTextView.text = ""
                    self.ocrTextView.isUserInteractionEnabled = false
                }

                alert.addTextField { (textField) in
                    textField.placeholder = NSLocalizedString("Enter File Name", comment: "")
                    textField.textColor = .black
                }
                alert.addAction(save)
                alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel) { (alertAction) in
                     self.ocrTextView.isUserInteractionEnabled = true
                })
                if( self.ocrTextView.text != "" && self.ocrTextView.text != nil) {
                    self.present(alert, animated:true, completion: nil)
                }
    }
        
    
    
    func TakePhotoOfDocument()  {
         
        
        // 1
        let imagePickerActionSheet =
          UIAlertController(title: NSLocalizedString("Choose File", comment: ""),
                            message: nil,
                            preferredStyle: .actionSheet)

        // 2
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
          let cameraButton = UIAlertAction(
            title: NSLocalizedString("Open Camera", comment: ""),
            style: .default) { (alert) -> Void in
                    self.showLoading(msg: "")
                    self.isOpenCameraView =  true
                    let imagePicker = UIImagePickerController()
                    imagePicker.delegate = self
                    imagePicker.sourceType = .camera
                    imagePicker.mediaTypes = [kUTTypeImage as String]
                    self.present(imagePicker, animated: true, completion: {
                        self.hideHud()
                    })
                
                  }
             
          imagePickerActionSheet.addAction(cameraButton)
        }

        // 3
        let libraryButton = UIAlertAction(
          title: NSLocalizedString("Choose from Gallery", comment: ""),
          style: .default) { (alert) -> Void in
                               self.showLoading(msg: "")
                                self.isOpenCameraView =  false
                               let imagePicker = UIImagePickerController()
                                imagePicker.delegate = self
                               imagePicker.sourceType = .photoLibrary
                               imagePicker.mediaTypes = [kUTTypeImage as String]
                               self.present(imagePicker, animated: true, completion: {
                                   self.hideHud()
                               })
            
        }
        imagePickerActionSheet.addAction(libraryButton)
        let cancelButton = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel)
        imagePickerActionSheet.addAction(cancelButton)
      
        
       if let popoverController = imagePickerActionSheet.popoverPresentationController {
         popoverController.sourceView = self.view
         popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
         popoverController.permittedArrowDirections = []
       }
        
        
        present(imagePickerActionSheet, animated: true)
    }
    
    
    // Tesseract Image Recognition
   @objc public  func performImageRecognition(_ image: UIImage) {
     
       let scaledImage = image.scaledImage(1000) ?? image
       let preprocessedImage = scaledImage.preprocessedImage() ?? scaledImage
       if let tesseract = G8Tesseract(language: "eng+fra") {
         tesseract.engineMode = .tesseractCubeCombined
         tesseract.pageSegmentationMode = .auto
         tesseract.image = preprocessedImage
         tesseract.recognize()
         var str : String = ocrTextView.text ?? ""
         str = str + (tesseract.recognizedText ?? "")
         self.ocrTextView.text = str
         self.hideHud()
        if(self.ocrTextView.text != "")
        {
            self.ocrTextView.isUserInteractionEnabled = true
        }
        let ss = tesseract.recognizedText
        if(ss == " \n\n" || ss == " \n\n\n" || ss == "" || ss == nil){
            Helper.showMessageView(title: NSLocalizedString("Error", comment: ""), message: NSLocalizedString("Data Parsing Failed", comment: ""), view: self)
        }
        
       }

     }
}



// MARK: - UINavigationControllerDelegate
extension OCRViewController: UINavigationControllerDelegate {
}

// MARK: - UIImagePickerControllerDelegate
extension OCRViewController: UIImagePickerControllerDelegate {
  func imagePickerController(_ picker: UIImagePickerController,
       didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
    guard let selectedPhoto =
      info[.originalImage] as? UIImage else {
        dismiss(animated: true)
        return
    }
    MBProgressHUD.showAdded(to: self.view, animated: true)
    if(Helper.getOCRCameraPhotoSaveInLibrary() && isOpenCameraView == true){
        
        UIImageWriteToSavedPhotosAlbum(selectedPhoto, self, #selector(imageSaveGalary(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    self.dismiss(animated: true, completion: {
       
        /*
         let swiftOCRInstance = SwiftOCR()
              
          swiftOCRInstance.recognize(selectedPhoto) { recognizedString in
             print("OCR Text Parsing Error")
              print(recognizedString)
            //  self.ocrTextView.text = recognizedString
          }
            */
          self.showLoading(msg: "")
         self.performImageRecognition(selectedPhoto)
         
          
    })
     
    
    
 
    
    
  }
    
    
    @objc func imageSaveGalary(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!
            let ac = UIAlertController(title: NSLocalizedString("Error", comment: ""), message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default))
           // present(ac, animated: true)
        } else {
            let ac = UIAlertController(title: NSLocalizedString("Save", comment: ""), message: NSLocalizedString("Image Saved in Gallery", comment: ""), preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default))
          //  present(ac, animated: true)
        }
    }
    
}

// MARK: - UIImage extension
extension UIImage {
  func scaledImage(_ maxDimension: CGFloat) -> UIImage? {
    var scaledSize = CGSize(width: maxDimension, height: maxDimension)

    if size.width > size.height {
      scaledSize.height = size.height / size.width * scaledSize.width
    } else {
      scaledSize.width = size.width / size.height * scaledSize.height
    }

    UIGraphicsBeginImageContext(scaledSize)
    draw(in: CGRect(origin: .zero, size: scaledSize))
    let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    
    return scaledImage
  }
  
  func preprocessedImage() -> UIImage? {
    let stillImageFilter = GPUImageAdaptiveThresholdFilter()
    stillImageFilter.blurRadiusInPixels = 15.0
    let filteredImage = stillImageFilter.image(byFilteringImage: self)
    return filteredImage
  }
}
