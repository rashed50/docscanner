//
//  PageContentViewController.h
//  DocScanner
//
//  Created by Rashed on 2017-03-10.
//  Copyright © 2017 Rashed. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PageContentViewController : UIViewController


@property (nonatomic) NSInteger  pageIndex;

@property NSString *imageFilePath;
@property NSString *imageDescription;
 
@property (strong, nonatomic) IBOutlet UIButton *removeBtnOutlet;

@property (strong, nonatomic) IBOutlet UILabel *lblImageDesciption;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;

- (IBAction)RemoveItemButtonAction:(id)sender;



@end
