//
//  PageContentViewController.m
//  DocScanner
//
//  Created by Rashed on 2017-03-10.
//  Copyright © 2017 Rashed. All rights reserved.
//

#import "PageContentViewController.h"

@interface PageContentViewController ()

@end

@implementation PageContentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
}

-(void)viewWillAppear:(BOOL)animated{

    [super viewWillAppear:animated];
    
    _removeBtnOutlet.hidden = NO;
    _imageView.image = [UIImage imageWithContentsOfFile:_imageFilePath];
    _imageView.contentMode = UIViewContentModeScaleAspectFit;
    if (_imageView.image == nil) {
        _removeBtnOutlet.hidden = YES;
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)RemoveItemButtonAction:(id)sender {
    
    NSLog(@"Removing ");
    [[NSNotificationCenter defaultCenter] postNotificationName:@"itemremoving" object:nil userInfo:@{@"index" : [NSString stringWithFormat:@"%ld",_pageIndex]}];
    
}
@end
