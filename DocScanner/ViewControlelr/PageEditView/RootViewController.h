//
//  RootViewController.h
//  DocScanner
//
//  Created by Rashed on 2017-03-10.
//  Copyright © 2017 Rashed. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PageContentViewController.h"
#import "CropViewController.h"
#import "MMCameraPickerController.h"



@interface RootViewController : UIViewController<UIPageViewControllerDelegate,UIPageViewControllerDataSource,UIImagePickerControllerDelegate,MMCameraDelegate,MMCropDelegate>


@property (strong, nonatomic) IBOutlet UIView *bottomContainerViewOutlet;


@property (nonatomic,strong) UIPageViewController *PageViewController;
@property (nonatomic,strong) NSMutableArray *allImageList;
@property (nonatomic,strong) NSMutableArray *originalImageList;
@property BOOL isLoadingFirstTime;
@property BOOL isLoadingCamera;

@property (nonatomic) NSInteger   pageIndex;

- (PageContentViewController *)viewControllerAtIndex:(NSUInteger)index;

- (IBAction)addMoreButtonAction:(id)sender;

- (IBAction)editButtonAction:(id)sender;

- (IBAction)uploadButtonAction:(id)sender;


@end
