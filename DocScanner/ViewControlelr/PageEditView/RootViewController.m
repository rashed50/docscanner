//
//  RootViewController.m
//  DocScanner
//
//  Created by Rashed on 2017-03-10.
//  Copyright © 2017 Rashed. All rights reserved.
//

#import "RootViewController.h"
#import "MBProgressHUD.h"
#import "DocumentScanner-Swift.h"
//#import "Helper/Constant.swift.h"
#import <AssetsLibrary/ALAsset.h>
#import <AVFoundation/AVAssetExportSession.h>
#import <AVFoundation/AVAsset.h>


@interface RootViewController ()
{
    UIImage *cropImage;
    Boolean isEditingEnable,isLoadingCompleted;
    UIImagePickerController *photoLibraryPicker;
    
}
@end

@implementation RootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemsRemovingAction:) name:@"itemremoving" object:nil];
   [self.navigationController.navigationBar setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"Georgia-Bold" size:18], NSFontAttributeName,[UIColor whiteColor],NSForegroundColorAttributeName,nil]];
    [self AddCustomNavigationBackButton];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [Helper setNavigationBarPropertyWithNavbar:self.navigationController size:18 title:NSLocalizedString(@"Document Scanning", @"")];
    
  //  [self.navigationController.navigationBar setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"Georgia-Bold" size:18], NSFontAttributeName,[UIColor whiteColor],NSForegroundColorAttributeName,nil]];
    //HelveticaNeue
    if (_allImageList.count == 0 && !_isLoadingFirstTime && !isLoadingCompleted){
        isLoadingCompleted  = YES;
        _allImageList = [NSMutableArray new];
        _originalImageList = [NSMutableArray new];
         [MBProgressHUD showHUDAddedTo:self.view animated:true];
       self.navigationItem.title = NSLocalizedString(@"Document Scanning", @"");
        if(_isLoadingCamera){
            [self openCameraView];
        }
        else{
            [self choosePhotosFromPhotoLibrary];
        }
    }
    
    
}

-(void)AddCustomNavigationBackButton{
    
    UIButton *btnBack =  [UIButton buttonWithType:UIButtonTypeCustom];
    [btnBack setFrame:CGRectMake(0, 0, 60, 40)];
    [btnBack setImage:[UIImage imageNamed:@"back64"] forState:UIControlStateNormal];
    [btnBack setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 40)];
    btnBack.showsTouchWhenHighlighted = YES;
    [btnBack addTarget:self action:@selector(NavigationBackBtnClick:)forControlEvents:UIControlEventTouchUpInside];
    [btnBack setContentMode:UIViewContentModeLeft];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)NavigationBackBtnClick:(UIButton*)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark Custom Methods

- (void) createViewController {
    PageContentViewController *vc = [self viewControllerAtIndex:self.allImageList.count -1];
    [self.PageViewController setViewControllers:@[vc] direction:UIPageViewControllerNavigationDirectionForward animated:true completion:nil];
    
}

-(void)itemsRemovingAction:(NSNotification*)notification
{
    NSDictionary *dic = notification.userInfo;
    int index = [[dic objectForKey:@"index"] intValue];
    NSLog(@"removing index is %d",index);
    self.pageIndex = index;
    
    if (self.allImageList.count == 1) {
        [self dismissViewControllerAnimated:YES completion:nil];
        return ;
    }
    
    if (self.pageIndex >= [self.allImageList count] - 1) {
        [self.allImageList removeObjectAtIndex:self.pageIndex];
        self.pageIndex -= 1;
        [self goToPreviousController:self.pageIndex];
    }else{
        [self.allImageList removeObjectAtIndex:self.pageIndex];
        [self goToNextController:index];
    }
}


- (void)goToNextController:(NSInteger)index {
    
    PageContentViewController *childViewController = [self viewControllerAtIndex:index];
    NSArray *viewControllers = [NSArray arrayWithObject:childViewController];
    [self.PageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
}

- (void)goToPreviousController:(NSInteger)index {
    
    UIViewController *childViewController = [self viewControllerAtIndex:index];
    NSArray *viewControllers = [NSArray arrayWithObject:childViewController];
    [self.PageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionReverse animated:YES completion:nil];
}

-(void)hideContentController:(UIViewController*)content {
    [content willMoveToParentViewController:nil];
    [content.view removeFromSuperview];
    [content removeFromParentViewController];
}

-(void)initializeAllLoadingData{
    
    
    self.PageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
    self.PageViewController.dataSource = self;
    PageContentViewController *startingViewController = [self viewControllerAtIndex:0];
    NSArray *viewControllers = @[startingViewController];
    
    
    [self.PageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    if(self.view.frame.size.height <= 667)
    self.PageViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.PageViewController.view.frame.size.height - _bottomContainerViewOutlet.frame.size.height);
    else
       self.PageViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.PageViewController.view.frame.size.height - 100);
    [self addChildViewController:_PageViewController];
    [self.view addSubview:_PageViewController.view];
    [self.PageViewController didMoveToParentViewController:self];
    
    
}

- (PageContentViewController *)viewControllerAtIndex:(NSUInteger)index
{
    if ([self.allImageList count] == 0 || index >= [self.allImageList count]) {
        return nil;
    }
    
    PageContentViewController *pageContentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageContentViewController"];
    pageContentViewController.imageFilePath = self.allImageList[index];
    pageContentViewController.pageIndex = (int) index;
    return pageContentViewController;
}


-(void)openCameraView
{
   
    UIStoryboard *mainboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MMCameraPickerController *cameraVC = [mainboard instantiateViewControllerWithIdentifier:@"mMCameraPickerController"];
    cameraVC.camdelegate = self;
    [Helper setNavigationBarPropertyWithNavbar:cameraVC.navigationController size:18 title: NSLocalizedString(@"Document Scanning", @"")];
    [self presentViewController:cameraVC animated:YES completion:^{
        
    }];
}



-(void)showCropViewControllerWithImage:(UIImage*)_cropImage isAutoCropEnable:(Boolean)isAutoCropEnable {
    
    CropViewController *cropViewCont = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"cropViewController"];
    
    cropViewCont.cropdelegate =    self;
    cropViewCont.adjustedImage = cropImage;
    cropViewCont.isAutoCropedEnable = isAutoCropEnable;
    [self presentViewController:cropViewCont animated:YES completion:nil];
    
    
}


-(void)closeButtonAction:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)addMoreButtonAction:(id)sender {

    [self addMoreFile];

    
}




- (IBAction)editButtonAction:(id)sender {
    
    NSLog(@"edit button click info %@",_originalImageList);
    if(_originalImageList.count == 0 || _pageIndex <0 || _pageIndex > _originalImageList.count)
        return ;
    UIImage *img = [UIImage imageWithContentsOfFile:_originalImageList[_pageIndex]];
    if(img != nil){
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            cropImage = img;
            isEditingEnable = true;
        [self showCropViewControllerWithImage:img isAutoCropEnable:NO];
        });
    }
     
}



-(void)addMoreFile{
    
    
    UIAlertController
    *actionSheet = [UIAlertController
    alertControllerWithTitle:NSLocalizedString(@"Scan More Hard File", @"") message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle: NSLocalizedString(@"Cancel" , @"") style:UIAlertActionStyleCancel                         handler:^(UIAlertAction *action) {
                            NSLog(@"Cancel button tapped");
                            [self dismissViewControllerAnimated:YES completion:^{}];
            }]];
    [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Scan Using Camera", @"")  style:UIAlertActionStyleDestructive
                                                  handler:^(UIAlertAction *action) {
                            NSLog(@"Deletebutton tapped");
                            [self openCameraView];
       
          }]];
    
    [actionSheet  addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Scan From Gallery", @"")   style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction *action) {
                            [self choosePhotosFromPhotoLibrary];
          }]];
    
    if([[[UIDevice.currentDevice.name substringToIndex:4] lowercaseString] isEqualToString:@"ipad"]){
        [[actionSheet popoverPresentationController] setSourceView:self.view];
        [[actionSheet popoverPresentationController] setSourceRect:CGRectMake(SCREEN_WIDTH/2, SCREEN_HEIGHT/2, 0, 0)];
        [[actionSheet popoverPresentationController] setPermittedArrowDirections:NO];
    }
    [self  presentViewController:actionSheet  animated:YES  completion:nil];
      
}



- (IBAction)uploadButtonAction:(id)sender {
   

    
    UIAlertController *alert = [UIAlertController
                               alertControllerWithTitle:  NSLocalizedString(@"Generating PDF File", @"")
                               message: NSLocalizedString(@"Enter File Name", @"")
                               preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *ok = [UIAlertAction actionWithTitle: NSLocalizedString(@"OK", @"") style: UIAlertActionStyleDefault
                                               handler:^(UIAlertAction *action){

                                        UITextField *alertTextField = alert.textFields.firstObject;
                                        NSString *filePath = [Helper saveImageAsPDFFile:self.allImageList fileName:alertTextField.text];
                                        NSLog(@"file written path %@",filePath);
                                       self.tabBarController.tabBar.hidden = false;
                                       [self.navigationController popToRootViewControllerAnimated:YES];

                                               }];

    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", @"") style:UIAlertActionStyleCancel
                                                   handler: nil];


    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {

        textField.placeholder = NSLocalizedString(@"Enter File Name", @"");

    }];

    [alert addAction:ok];
    [alert addAction:cancel];

    if(_originalImageList.count > 0)
       [self presentViewController:alert animated:YES completion:nil];
     

}
 




#pragma mark   MMCameraPickerController Delegate

-(void)didFinishCaptureImage:(UIImage *)capturedImage withMMCam:(MMCameraPickerController *)cropcam
{
    
    cropImage = capturedImage;
    [cropcam dismissViewControllerAnimated:YES completion:^{

        if( capturedImage == nil ){
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        }else {
            
           NSString *filePath = [Helper saveOriginalImage:[NSString stringWithFormat:@"%ld",self.allImageList.count + 1] imgFile:capturedImage];
            isEditingEnable = false;
          //  UIImage *img = [UIImage imageWithContentsOfFile:filePath];
           [_originalImageList addObject:filePath];
            [self showCropViewControllerWithImage:capturedImage isAutoCropEnable: [Helper getUserDefaultPreference]];
        }
    }];
}
-(void)authorizationStatus:(BOOL)status {
    
}

#pragma mark   IMAGE CROPING  Delegate
-(void)didFinishCropping:(UIImage *)finalCropImage from:(CropViewController *)cropObj{
    
   
    [self dismissViewControllerAnimated:NO completion:^{
        
        NSString *filePath ;
        
        if ( isEditingEnable) {
            
            filePath = [Helper saveImageAfterEditing:[NSString stringWithFormat:@"%ld",_pageIndex] imgFile:finalCropImage];
            [self.allImageList replaceObjectAtIndex:_pageIndex withObject:filePath];
            cropImage = finalCropImage;
            isEditingEnable = false;
            
        }else{
            
            filePath = [Helper saveImageAfterEditing:[NSString stringWithFormat:@"%d",(int)self.allImageList.count] imgFile:finalCropImage];
            [self.allImageList addObject:filePath];
        }
        
        if (!_isLoadingFirstTime && isLoadingCompleted) {
            _isLoadingFirstTime = true;
            [self initializeAllLoadingData];
        }else {
            [self createViewController];
        }
        
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        
    }];
    
    
}




#pragma mark PageViewController Need this for page indicator

-(NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    NSLog(@"total list %lu",(unsigned long)[self.allImageList count]);
    return [self.allImageList count];
}
/*
 - (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
 {
 return 0;
 }
 
 */



#pragma mark PageViewController Delegate Methods

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    
    NSUInteger index = ((PageContentViewController*) viewController).pageIndex;
    _pageIndex = index == NSNotFound ? 0 :index;
    if ((index == 0) || (index == NSNotFound))
    {
        return nil;
    }
    index--;
    
    NSLog(@"Before index value %ld  and %ld",index , _pageIndex);
    return [self viewControllerAtIndex:index];
    
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    
    NSUInteger index = ((PageContentViewController*) viewController).pageIndex;
    _pageIndex = index == NSNotFound ? 0 :index;
    if (index == NSNotFound)
    {
        return nil;
    }
    index++;
    NSLog(@"After index value %lu //  %ld",(unsigned long)index,_pageIndex);
    return [self viewControllerAtIndex:index];
    
}

 
#pragma Select Photo From  Gallary


- (void)choosePhotosFromPhotoLibrary {

    [MBProgressHUD showHUDAddedTo:self.view animated:true];
     photoLibraryPicker = [[UIImagePickerController alloc] init];
    photoLibraryPicker.delegate =self; //id<UIImagePickerControllerDelegate> ;
    photoLibraryPicker.allowsEditing = NO;
    photoLibraryPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [Helper setNavigationBarPropertyWithNavbar:photoLibraryPicker.navigationController size:18 title: @"Choose Photo"];
    [self presentViewController:photoLibraryPicker animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {

    UIImage *capturedImage = info[UIImagePickerControllerOriginalImage];
       cropImage = capturedImage;
       [picker dismissViewControllerAnimated:YES completion:^{
           
           if( capturedImage == nil ){
               [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
           }else {
               
              NSString *filePath = [Helper saveOriginalImage:[NSString stringWithFormat:@"%ld",self.allImageList.count + 1] imgFile:capturedImage];
               isEditingEnable = false;
             //  UIImage *img = [UIImage imageWithContentsOfFile:filePath];
              [_originalImageList addObject:filePath];
               [self showCropViewControllerWithImage:capturedImage isAutoCropEnable:true];
           }
       }];
    
    
    
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
     [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
     [self dismissViewControllerAnimated:YES completion:nil];
}















@end
