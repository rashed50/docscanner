//
//  InitialViewController.swift
//  Doc Scanner
//
//  Created by Rashed on 2017-01-24.
//  Copyright © 2017 iRLMobile. All rights reserved.
//

import UIKit
import GoogleMobileAds

class ScanViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,GADBannerViewDelegate {

    
    var listDictionaryData :NSMutableDictionary = NSMutableDictionary()
    
     var adMobBannerView: GADBannerView!
    
    
    @IBOutlet var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("Scan", comment: "")
       initializeGoogleAdmob()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.tableView.delegate = self
        self.loadTableData()
        Helper.setNavigationBarProperty(navbar: self.navigationController!, size: 18, title:NSLocalizedString("Document Scanning", comment: ""))
        self.tabBarController?.tabBar.isHidden = false
         adMobBannerView.isHidden = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
     }
 //
// MARK TABLE VIEW DELEGATE METHODS
//
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:UITableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "cell1")!
        
        let sectionList = Array<String>(listDictionaryData.allKeys as! Array)
        let sectiontitle = sectionList[indexPath.section] as String
        let   arrayList = Array<String>(listDictionaryData.object(forKey:sectiontitle) as! Array)
       
       cell.textLabel?.text = NSLocalizedString(arrayList[indexPath.row], comment: "")
        return cell
        
        
    }
    func tableView( _ tableView : UITableView,  titleForHeaderInSection section: Int)->String? {
        let sectionList = Array<String>(listDictionaryData.allKeys as! Array)
        let sectiontitle = sectionList[section] as String
        return NSLocalizedString(sectiontitle, comment: "")
        
    }
      func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let sectionList = Array<String>(listDictionaryData.allKeys as! Array)
        let sectiontitle = sectionList[section] as String
        let   arrayList = Array<String>(listDictionaryData.object(forKey:sectiontitle) as! Array)
        return arrayList.count;
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let sectionList = Array<String>(listDictionaryData.allKeys as! Array)
        let sectiontitle = sectionList[indexPath.section] as String
        //let   arrayList = Array<String>(listDictionaryData.object(forKey:sectiontitle) as! Array)
        if sectiontitle == "Camera" {
            if(indexPath.row == 0){
                self.openCameraViewAction()
            }
            else if(indexPath.row == 1){
                 var scan2SideCon : Scan2SideViewController
                let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
                if #available(iOS 13.0, *) {
                      scan2SideCon  = storyboard.instantiateViewController(identifier: "scan2SideViewController")
                } else {
                    scan2SideCon  = storyboard.instantiateViewController(withIdentifier: "scan2SideViewController") as! Scan2SideViewController
                }
                self.navigationController?.pushViewController(scan2SideCon, animated: true)
            }
        }
        else{
            self.openPhotoLibary()
        }
    }
    
    
    func loadTableData() {
        
        var list1 :Array = Array<String>()
        list1.append("Scan From Gallery")
       // list1.append("2")
        listDictionaryData.setObject(list1, forKey: "Gallery" as NSCopying)
        var list2 :Array = Array<String>()
        list2.append("Scan Using Camera")
        list2.append("Scan 2 Side Document")
      //  list2.append("3")
       // list2.append("4")
        listDictionaryData.setObject(list2, forKey: "Camera" as NSCopying)
        
        print(listDictionaryData)
        
    }
    
    
    func openCameraViewAction() {
        

        let viewController :RootViewController = UIStoryboard(name:"Main" , bundle: nil).instantiateViewController(withIdentifier: "rootViewController") as! RootViewController
        viewController.isLoadingFirstTime = false
         viewController.isLoadingCamera = true
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.pushViewController(viewController, animated:true)
       
   
        
    }
    
    func openPhotoLibary() {
         
        let viewController :RootViewController = UIStoryboard(name:"Main" , bundle: nil).instantiateViewController(withIdentifier: "rootViewController") as! RootViewController
              viewController.isLoadingFirstTime = false
              viewController.isLoadingCamera = false
              self.tabBarController?.tabBar.isHidden = true
              self.navigationController?.pushViewController(viewController, animated:true)
         
    }
    
    func openIRScannerView()  {
        
        /*
        let scanner = IRLScannerViewController.standardCameraView(with: self as! IRLScannerViewControllerDelegate)
        scanner.showControls = true
        scanner.showAutoFocusWhiteRectangle = true
       // self.editingButtonOutlet.isHidden = false
        present(scanner, animated: true, completion: {
         //   self.scrollViewContainer.isHidden = true
        })
        
        */
    }
    
    func initializeGoogleAdmob() {
                  
           adMobBannerView = GADBannerView(adSize: kGADAdSizeBanner)
           adMobBannerView.adUnitID = Helper.getAdMobBannerID()
           adMobBannerView.rootViewController = self
           adMobBannerView.delegate = self as GADBannerViewDelegate
           adMobBannerView.load(GADRequest())
           adMobBannerView.isHidden = true
           addBannerInViewToBottom()
             
    }
    
    func addBannerInViewToBottom() {
        
        adMobBannerView.frame = CGRect(x: 0.0,
                                       y: self.view.frame.height - adMobBannerView.frame.height-100,
                                                   width: self.view.frame.width,
                                 height: adMobBannerView.frame.height)
     
        self.view.addSubview(adMobBannerView)

    }
       
//
  // MARK GOOGLE ADMOB BANNER DELEGATE
  //
  
  /// Tells the delegate an ad request failed.
     func adView(_ bannerView: GADBannerView,
         didFailToReceiveAdWithError error: GADRequestError) {
       print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
     }

     /// Tells the delegate that a full-screen view will be presented in response
     /// to the user clicking on an ad.
     func adViewWillPresentScreen(_ bannerView: GADBannerView) {
       print("adViewWillPresentScreen")
     }

     /// Tells the delegate that the full-screen view will be dismissed.
     func adViewWillDismissScreen(_ bannerView: GADBannerView) {
       print("adViewWillDismissScreen")
     }

     /// Tells the delegate that the full-screen view has been dismissed.
     func adViewDidDismissScreen(_ bannerView: GADBannerView) {
       print("adViewDidDismissScreen")
     }

     /// Tells the delegate that a user click will open another app (such as
     /// the App Store), backgrounding the current app.
     func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
       print("adViewWillLeaveApplication")
     
     }
    
}



