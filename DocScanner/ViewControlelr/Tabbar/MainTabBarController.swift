//
//  MainTabBarController.swift
//  Doc Scanner
//
//  Created by Rashed on 2017-01-19.
//  Copyright © 2017 iRLMobile. All rights reserved.
//

import UIKit

class MainTabBarController: UITabBarController,UITabBarControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        self.tabBarController?.selectedIndex = 2
       // self.title = Bundle.main.localizedString(forKey: "Scan", value: "", table: "Localizable")
   
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
    }
       func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
                print("going to selected tabbar items")
        
        return true
     }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
         print("selected index %d",tabBarController.selectedIndex)
        if tabBarController.selectedIndex == 0 {
             self.title = Bundle.main.localizedString(forKey: "Download", value: "", table: "Localizable")
        }
    }
    
 
}
