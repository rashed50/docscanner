//
//  ViewController.swift
//  demo
//
//  Created by Denis Martin on 28/06/2015.
//  Copyright (c) 2015 iRLMobile. All rights reserved.
//

import UIKit


class ViewController : UIViewController, MMCropDelegate ,MMCameraDelegate {
  


    let SCREEN_WITH = Int(UIScreen.main.bounds.size.width)
    let SCREEN_HEIGHT = Int(UIScreen.main.bounds.size.height)

    var cropImage : UIImage? = nil
    var isEditingEnable :Bool = false
    var isLoadingFirstTime :Bool = false
    
    
    var documentList: [String] = []
    
    @IBOutlet var scrollViewContainer: UIScrollView!
    
    @IBOutlet weak var scanButton: UIButton!
   
    @IBOutlet var editingButtonOutlet: UIButton!
    
    
    override func viewDidLoad() {

        Helper.createDirectory()
      

    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("total scan doc %d   %d",self.documentList.count, isLoadingFirstTime)
        if documentList.count == 0 && !isLoadingFirstTime {
            isLoadingFirstTime = true
           openCameraView()
        }
    }
    
    func openCameraView() {
        
        /*
         let scanner = IRLScannerViewController.standardCameraView(with: self)
         scanner.showControls = true
         scanner.showAutoFocusWhiteRectangle = true
         self.editingButtonOutlet.isHidden = false
         present(scanner, animated: true, completion: {
         self.scrollViewContainer.isHidden = true
         })
         */
        
        let mmCameraPicker : MMCameraPickerController = UIStoryboard(name:"Main", bundle : nil).instantiateViewController(withIdentifier: "mMCameraPickerController") as! MMCameraPickerController
        mmCameraPicker.camdelegate =  self
        
        
        present(mmCameraPicker, animated: false, completion: {
            self.scrollViewContainer.isHidden = true
        })
        
    }
    
    
      func getDestinationPath(_ fileName: String) -> String {

          let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let filePath = "\(documentsPath)/\(fileName).pdf"
        return filePath
    }
    
    func loadPDF(_ filePath: String) {
        
        let url = NSURL(fileURLWithPath: filePath)
        let urlRequest = NSURLRequest(url: url as URL)
        let pdfView = UIWebView()
        pdfView.frame = self.scrollViewContainer.frame
        pdfView.loadRequest(urlRequest as URLRequest)
        self.view.addSubview(pdfView)
    }

    // MARK: BUTTON ACTIONS
    
    
    @IBAction func imageEditButtonAction(_ sender: Any) {
        
        if cropImage != nil
        {
            self.editingButtonOutlet.isHidden = true
            self.isEditingEnable = true;
            self.showCropViewController(cropImg: cropImage! , isAutoCropEnable:false)

        }
    }
    
    
    @IBAction func saveImagesAsPDFFile(_ sender: Any) {
      
        
        let alert = UIAlertController(title: "File Name", message: "Please enter file name", preferredStyle:
            UIAlertController.Style.alert)
        
        alert.addTextField()
        
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler:{ (UIAlertAction)in
            
            
            
            let textField = alert.textFields![0]
            var filename = String()
            filename = textField.text!;
            if !filename.isEmpty
            {
                MBProgressHUD.showAdded(to: self.view, animated: true);
                let dst = Helper.getFileSavingDestinationPath(filename, fileExtention: "pdf")
            //    getFileSavingDestinationPath(filename)
                
                do {
                    let pdfData = try PDFGenerator.generated(by: self.documentList.reversed())
                    try pdfData.write(to: URL(fileURLWithPath: dst) )
                    print("PDF file destination \(dst)");
                    //self.loadPDF(dst)
                    self.documentList.removeAll()
                  //  Helper.get()
                }
                catch (let e) {
                    print("data wirting error is %@",e)
                }
                MBProgressHUD.hideAllHUDs(for: self.view as UIView, animated: true)
                self.dismiss(animated: true, completion:nil)
                
            }
            
        }))
        self.present(alert, animated: true, completion:nil)
    }
    
    @IBAction func scan(_ sender: AnyObject) {
       
        let subViews = self.view.subviews
        for subview in subViews{
            if (subview is UIWebView) {
                subview.removeFromSuperview()
                break
            }
        }
      self.openCameraView()
    }
    
    /*
    // MARK: IRLScannerViewControllerDelegate
    func pageSnapped(_ page_image: UIImage, from controller: IRLScannerViewController) {
        controller.dismiss(animated: true) { () -> Void in
            
           
           //  self.documentList.insert(page_image, at:0)
            print("all info %d",self.documentList.count)
           /* if self.documentList.count >= 1
            {
                self.scanButton.setTitle("Scan Another", for: UIControlState.normal)
            }*/
            self.setContentToScrollView()
        }
    }
    
    func showImageCropView(_ page_image: UIImage, from controller: IRLScannerViewController) {
      
        MBProgressHUD.showAdded(to: self.view, animated: true)
           cropImage = page_image
         controller.dismiss(animated: true) { () -> Void in
            self.isEditingEnable = false
            self.showCropViewController(cropImg: page_image , isAutoCropEnable: true)
        }
    }
    
    func didCancel(_ cameraView: IRLScannerViewController) {
        
        self.editingButtonOutlet.isHidden = true
        cameraView.dismiss(animated: true){ ()-> Void in
            NSLog("Cancel Image Editing");
        }
    }
     
     func cameraViewWillUpdateTitleLabel(_ cameraView: IRLScannerViewController) -> String? {
     
     var text = ""
     switch cameraView.cameraViewType {
     case .normal:           text = text + "NORMAL"
     case .blackAndWhite:    text = text + "B/W-FILTER"
     case .ultraContrast:    text = text + "CONTRAST"
     }
     
     switch cameraView.detectorType {
     case .accuracy:         text = text + " | Accuracy"
     case .performance:      text = text + " | Performance"
     }
     print("calling text change %@",text);
     return text
     }
     
     
    // MARK:End IRLScannerViewControllerDelegate
    
    
    */
    func showCropViewController(cropImg: UIImage,isAutoCropEnable:Bool) {
        
        self.isLoadingFirstTime = true
        let cropViewCont:CropViewController    = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "crop") as! CropViewController
        
        cropViewCont.cropdelegate = self;
        // ripple.touchPoint = self.view.;
        cropViewCont.adjustedImage = cropImg;
        //cropViewCont.isAutoCropedEnable = isAutoCropEnable;
        present(cropViewCont, animated: true, completion: nil)
        
    }
    func setContentToScrollView() {
        
        
        self.scrollViewContainer.subviews.forEach({ $0.removeFromSuperview() })
        
        let imageWidth:CGFloat = CGFloat(self.scrollViewContainer.frame.size.width)
        let imageHeight:CGFloat = CGFloat(self.scrollViewContainer.frame.size.height)
        var xPosition:CGFloat = 0
        var scrollViewSize:CGFloat=0
        
        for imagePath in self.documentList {
            
            
            let myImageView:UIImageView = UIImageView()
            myImageView.image = UIImage.init(contentsOfFile: imagePath)!
            //Helper.getImageFromTemporaryFilePath("\(image)")
            myImageView.frame.size.width = imageWidth
            myImageView.frame.size.height = imageHeight
            myImageView.frame.origin.x = xPosition
            myImageView.frame.origin.y = 0
            
            self.scrollViewContainer.addSubview(myImageView)
            self.scrollViewContainer.showsHorizontalScrollIndicator = false
            xPosition += imageWidth + 10
            scrollViewSize += imageWidth + 10
        }
         self.scrollViewContainer.isHidden = false
         self.scrollViewContainer.isScrollEnabled = true;
         self.scrollViewContainer.contentSize = CGSize(width:scrollViewSize, height: imageHeight)
        
    }
        
  

   
    
  //MARK: CropViewController Delegate
    func didFinishCropping(_ finalCropImage: UIImage , from:CropViewController) {
        
       
        from.dismiss(animated: true) { () -> Void in
            
        
            if self.isEditingEnable {
                self.documentList.remove(at: 0);
                self.cropImage = nil
                self.editingButtonOutlet.isHidden = true
            }else{
                self.editingButtonOutlet.isHidden = false
            }
            
            let indexValue = self.documentList.count
            self.documentList.insert(Helper.getTemporaryFileDestinationPath( "\(indexValue)" ,imgFile: finalCropImage), at:0)
            Helper.getTemporaryFileDestinationPath( "\(indexValue)" ,imgFile: finalCropImage)
            
          //  print("all info %d",self.documentList.count)
          /*  if self.documentList.count >= 1
            {
                self.scanButton.setTitle("Scan Another", for: UIControlState.normal)
            }*/
            self.setContentToScrollView()
            MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
            
        }
        
    }
    
    
  //MARK: MMCameraPickerController Delegate
    @available(iOS 2.0, *)
    public func didFinishCapture(_ capturedImage: UIImage!, withMMCam cropcam: MMCameraPickerController!) {
       
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        cropImage = capturedImage
        cropcam.dismiss(animated: true) { () -> Void in
            
            if capturedImage == nil {
                MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
                 self.scrollViewContainer.isHidden = false
            }
            else {
                self.isEditingEnable = false
                self.showCropViewController(cropImg: capturedImage , isAutoCropEnable: true)
            }
        }
        
    }
    
    func authorizationStatus(_ status: Bool) {
        
    }
   // end delegate
    
    
}



