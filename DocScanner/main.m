//
//  main.m
//  DocScanner
//
//  Created by Rashed on 2017-01-16.
//  Copyright © 2017 Rashed. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
